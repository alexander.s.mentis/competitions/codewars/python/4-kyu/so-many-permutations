import itertools

def permutations(s):
    # Code Away!
    return list({''.join(_) for _ in itertools.permutations(s)})